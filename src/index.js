import TelegramBot from 'node-telegram-bot-api';
import {error, log} from './lib/utils/log';
import dotenv from 'dotenv-safe';
import pjson from 'prettyjson';
import Searcher from './lib/Searcher';

dotenv.load();

const bot = new TelegramBot(process.env.TELEGRAM_TOKEN, {
    polling: true
});

const searcher = new Searcher();

bot.getMe().then(me => {
    const _info = [];
    _info.push('------------------------');
    _info.push('Bot successfully started');
    _info.push(`Username: ${me.username}`);
    _info.push('------------------------');
    log(_info.join('\n'));
});

bot.onText(/(?:\/[^\s]+)? ?(.*)/, (msg, match) => {
    bot.sendMessage(
        msg.chat.id,
        'Ops... Currently, I only work on inline mode.',
        {
            reply_markup: {
                inline_keyboard: [
                    [{text: 'Go inline', switch_inline_query: match[1]}]
                ]
            }
        }
    );
});

const answerInlineQuery = (id, res) => {
    bot.answerInlineQuery(id, res).catch(err => error(pjson.render(err)));
};

bot.on('inline_query', qry => {
    if (qry.id && qry.query) {
        searcher
            .search(qry.query)
            .then(res => answerInlineQuery(qry.id, res))
            .catch(err => error(pjson.render(err)));
    }
});
