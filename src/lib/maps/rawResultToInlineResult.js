export default function(item) {
    const inlineQueryResult = {
        type: 'article',
        id: item.cacheId,
        title: item.title,
        description: item.snippet,
        url: item.link,
        input_message_content: {
            message_text: `[${item.title}](${item.link})\n${item.snippet}`,
            parse_mode: 'Markdown'
        }
    };

    return inlineQueryResult;
}
