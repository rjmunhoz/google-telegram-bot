/* eslint-disable no-console */

import chalk from 'chalk';

const checkEnv = () => {
    switch (process.env.NODE_ENV) {
    case 'test':
    case 'silent':
        return false;
    default:
        return true;
    }
};

const consolelog = (text, color) => {
    if (checkEnv()) {
        console.log(color(text));
    }
};

const log = text => consolelog(text, chalk.blue);

const info = text => consolelog(text, chalk.cyan);

const warn = text => consolelog(text, chalk.yellow);

const error = text => consolelog(text, chalk.red);

export default {
    log,
    info,
    warn,
    error
};

export {log, info, warn, error, checkEnv};
