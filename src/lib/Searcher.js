import maps from './maps';
import request from 'request-promise-native';

export default class Searcher {
    constructor() {
        this.API_URL = 'https://www.googleapis.com/customsearch/v1';
    }

    search(terms, map, _KEY, _CX) {
        map = map || maps.rawResultToInlineResult;

        const KEY = _KEY || process.env.GOOGLE_KEY;
        const CX = _CX || process.env.GOOGLE_CX;

        return new Promise((ok, notOk) => {
            request(`${this.API_URL}`, {
                json: true,
                qs: {
                    q: terms,
                    key: KEY,
                    cx: CX
                }
            })
                .then(res => {
                    ok(res.items.map(map));
                })
                .catch(notOk);
        });
    }
}
