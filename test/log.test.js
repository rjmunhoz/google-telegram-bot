/* global describe, it */
import {expect} from 'chai';
import {checkEnv} from '../src/lib/utils/log';

describe('Log Utils', function() {
    it('checkEnv() returns true for normal NODE_ENV', function() {
        const oldenv = process.env.NODE_ENV;
        delete process.env.NODE_ENV;
        expect(checkEnv()).to.equal(true);
        process.env.NODE_ENV = oldenv;
    });
    it('checkEnv() should return false for testing NODE_ENV', function() {
        expect(checkEnv()).to.equal(false);
    });
});
