/* global describe, it, before */
import {expect} from 'chai';
import Searcher from '../src/lib/Searcher';
import dotenv from 'dotenv-safe';

dotenv.load();

describe('Searcher', function() {
    let searcher;
    let hits;

    before(function(done) {
        this.timeout(10000);
        searcher = new Searcher();
        searcher.search('nodejs').then(_hits => {
            hits = _hits;
            done();
        });
    });

    it('Hits should be an array', function() {
        expect(hits).to.be.an('array');
    });

    describe('Hits should be an InlineQueryResultArticle', function() {
        it('Type should be \'article\'', function() {
            expect(hits[0].type).to.equal('article');
        });

        it('Should have ID', function() {
            expect(hits[0].id).to.exist;
        });

        it('Should have title', function() {
            expect(hits[0].title).to.exist;
        });

        it('Should have description', function() {
            expect(hits[0].description).to.exist;
        });

        it('Should have url', function() {
            expect(hits[0].url).to.exist;
        });

        it('Should have input_message_content', function() {
            expect(hits[0].input_message_content).to.exist;
        });

        describe('Should have InputTextMessageContent', () => {
            it('Should have message_text', function() {
                expect(hits[0].input_message_content.message_text).to.exist;
            });

            it('Should have parse_mode', function() {
                const parse_mode = hits[0].input_message_content.parse_mode;
                expect(parse_mode).to.equal('Markdown');
            });
        });
    });
});
